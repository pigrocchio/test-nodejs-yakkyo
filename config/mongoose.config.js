import { connect } from 'mongoose'
// importo il modulo connect di mongoose

// definisco l'url per il collegamento al mio DB  (Adesso in locale)
const url = 'mongodb://127.0.0.1:27017/yakkyoprovanode'

// const url = "mongodb://test:yakkyo@ds135069.mlab.com:35069/test-yakkyo'"

/* connetto l'istanza di mongo al DB, primo argomento passo l'url,
gli altri argomenti sono opzionali per evitirare i DeprecationWarning in console*/
connect(url, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
})
  .then(() => console.log('Connected to database', url))
  .catch((err) => console.log('"Error connecting to mongo"', err))

module.exports = connect
