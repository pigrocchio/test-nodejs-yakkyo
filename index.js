
require('./config/mongoose.config')
// importo la config di mongoose

import bodyParser from 'body-parser'
import express from 'express'
import cors from 'cors'

// creo un istanza di express dentro la variabile app
const app = express()

app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
/*  app.use mi permette di utilizzare dei middleware a livello di tutta la 
app, in questo caso per tutti gli endpoint utilizzo il modulo
cors: permette petizioni http da indirizzi con origini diverse
bodyparser: mi permette di accere i dati del body delle richieste post e put in formato json*/

app.use('/', require('./routes/User.routes'))
/* utilizzo il middleware per agganciarmi ai sub-endpoint di
User.Route per tutte le richieste che provengono da "/"*/

/*  avvio il server, express istanzia il modulo http.create server e rimane in 
ascolto  sulla porta specificata (8080) di richieste su i relativi endpoint */
app.listen(8080, () => console.log('Example app listening on port 8080!'))
