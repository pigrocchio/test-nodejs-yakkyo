import UserModel from '../model/UserModel'
// importo il modello di mongo richiesto per le operazioni sugli endpoint

import express from 'express'
//importo il modulo express

const router = express.Router()
/* utilizzo un middleware a livello di router per avere 
delle subroutes e codice più ordinato*/

//entry endpoint
router.get('/', (req, res) => {
  res.send('Just a test')
})

//handler route per metodi con stesso endpoint ma metodi differenti
router
  .route('/users')
  .get((req, res) => {
    UserModel.find((err, results) => {
      res.send(results)
    })
  })
  .post((req, res) => {
    const { email, firstName, lastName, password } = req.body
    //destrutturo l'oggetto req.body in singole variabili

    //istanzio un nuovo modello di usuario e passo gli dati
    let user = new UserModel({ email, firstName, lastName, password })
    user.save((err, newUser) => {
      if (err) res.send(err)
      else res.send(newUser)
    })
  })

// Update User

router.route('/users/:id') //utilizzo req.params per catturare l'id utente dalla url
  .put(async (req, res) => {
    
    try {
      const user = await UserModel.findById(req.params.id)
      //controllo se esiste l'utente con l'id specificato e lo salvo
      user.set(req.body)
      /* uso  set di mongoose per aggiornare 
        le propietà corrispondeti al modello con i valori che ricevo 
        dal body*/
        
      const result = await user.save() //savlo i dati aggiornati nel DB
      res.send(result)
    } catch (err) {
      res.status(500).send({ message: 'error user not found' })
    }
  })
    .delete(async (req, res) => {
    try {
        const user = await UserModel.findByIdAndDelete(req.params.id)
        if (!user) res.status(404).send('No user found')
        res.status(200).send('User deleted')
    } catch (err) {
        res.status(500).send(err)
    }
    })

    module.exports = router